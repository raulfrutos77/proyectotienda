import mongoose from 'mongoose';
import {Clients} from './db';
import { rejects } from 'assert';

export const resolvers ={
    Query:{
        getClients: (root, {limit})=>{
            return Clients.find({}).limit(limit)
        },
        getClient: (root, {id})=>{
            return new Promise((resolve, objetc)=>{
                Clients.findById(id, (error, client)=>{
                    if(error) rejects(error)
                    else resolve(client)
                });
            });
        },
    },
    Mutation:{
        createClient: (root, {input}) =>{
            const newClient= new Clients({
                
                name: input.name,
                surname: input.surname,
                company: input.company,
                emails: input.emails,
                age: input.age,
                type: input.type,
                requests: input.requests
            });
            newClient.id= newClient._id;
            
            return new Promise((resolve, objetc)=>{
                newClient.save((error)=> {
                    if(error) rejects(error)
                    else resolve( newClient)
                })
            })
        },
        updateClient: (root, {input})=>{
            return new Promise((resolve, object)=>{
                Clients.findOneAndUpdate({_id : input.id }, input, {new:true}, (error,client)=> {
                    if(error) rejects(error)
                    else resolve(client)
                });
            });
        },
        deleteClient: (root, {id})=>{
            return new Promise((resolve, object)=>{
                Clients.findOneAndDelete({_id : id }, (error)=> {
                    if(error) rejects(error)
                    else resolve("Se eliminó correctamente")
                });
            });
        },   
    }
}
